-- phpMyAdmin SQL Dump
-- version 5.0.0-dev
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 20, 2018 at 01:27 AM
-- Server version: 5.5.59
-- PHP Version: 7.1.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `body` varchar(500) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `title`, `body`, `updated_at`, `created_at`) VALUES
(2, 'Ut in alias non natus odio cumque similique in.', 'Aut minima aut sint autem omnis fuga quo. Accusamus delectus sed illum quas. Et et sit sint qui est error. Cum soluta dolorum quis praesentium veritatis ab alias.', '2018-11-13 02:10:24', '2018-11-13 02:10:24'),
(4, 'Sit fugiat consequuntur aspernatur quae.', 'Rerum et ad voluptate eum dolorum molestiae et. Voluptatem occaecati qui commodi illo sequi omnis. Et tempora deserunt possimus non.', '2018-11-13 02:10:25', '2018-11-13 02:10:25'),
(5, 'Voluptatem quia omnis sit voluptas molestiae.', 'Omnis excepturi architecto et molestias. Placeat tempore sed dicta soluta. Tempore error nihil qui qui. Maxime rem eum provident sit molestias minima placeat nam. Eum sunt et ea quaerat.', '2018-11-13 02:10:25', '2018-11-13 02:10:25'),
(6, 'Aliquam sit ratione odio doloremque et.', 'Illum sunt vel mollitia non dolorum esse enim. Cupiditate nobis est est similique dolorem.', '2018-11-13 02:10:25', '2018-11-13 02:10:25'),
(7, 'Qui nostrum eius atque quas.', 'Sed sint dolore ut eaque a hic. Quis quisquam eos alias ipsum quaerat. Dolores sint et quidem odit. Id repellendus vitae numquam quas. Ut quidem dicta voluptatem ratione delectus voluptatum.', '2018-11-13 02:10:25', '2018-11-13 02:10:25'),
(8, 'Debitis suscipit reprehenderit nesciunt maiores minima amet et.', 'Sit accusantium et non. Occaecati alias nam labore. Autem sit quia enim culpa id. Aut quia ut voluptatem odit.', '2018-11-13 02:10:25', '2018-11-13 02:10:25'),
(9, 'Harum iste hic nostrum ut perferendis alias.', 'Eos autem non ducimus ipsum quo harum. Sed in eligendi libero aut hic sunt quas. Repudiandae doloribus enim necessitatibus consequatur.', '2018-11-13 02:10:25', '2018-11-13 02:10:25'),
(10, 'Occaecati soluta est facere.', 'Ducimus et ad rem neque voluptas eligendi. Reprehenderit rem eius nulla est. Et molestias impedit distinctio voluptatem. At distinctio ex in doloribus ut.', '2018-11-13 02:10:26', '2018-11-13 02:10:26'),
(11, 'Voluptatem quo quas ea praesentium consequatur.', 'Id ad eos nisi qui error. Enim consequuntur ipsa occaecati neque sit. Facilis possimus ipsa deserunt aut maxime rem.', '2018-11-13 02:10:26', '2018-11-13 02:10:26'),
(12, 'Esse culpa deserunt est deleniti excepturi qui quia.', 'Omnis ut tenetur asperiores magnam expedita iste in. Voluptas excepturi repellendus enim omnis reiciendis. Accusantium accusantium unde corporis et dicta vel.', '2018-11-13 02:10:26', '2018-11-13 02:10:26'),
(13, 'Ipsum eveniet blanditiis nihil exercitationem.', 'Est eum corporis maxime sed officia ipsa. Reiciendis rem consequuntur tenetur veritatis repellendus. Impedit beatae rerum ut dicta consequatur dolores.', '2018-11-13 02:10:26', '2018-11-13 02:10:26'),
(14, 'Accusamus sint eveniet quo et velit consequatur minima neque.', 'Voluptatem deleniti ipsum corporis vitae minus dolorum. Eligendi et ut ad blanditiis odit iste recusandae. Ad quidem temporibus saepe.', '2018-11-13 02:10:26', '2018-11-13 02:10:26'),
(15, 'Vitae ut consequatur quo dignissimos sint autem vel.', 'Illo consequuntur voluptate dolor voluptatibus officiis quia et. Ut ratione dolore sit distinctio alias aut ex ex. Fuga est qui et fugiat beatae est.', '2018-11-13 02:10:26', '2018-11-13 02:10:26'),
(16, 'Nisi eum maxime sit sit totam.', 'Vel sit unde at vel. Vel quia necessitatibus saepe.', '2018-11-13 02:10:26', '2018-11-13 02:10:26'),
(17, 'Quis placeat qui aut iure numquam sunt aut.', 'Expedita animi voluptatum sit iste. Omnis aliquid porro molestias. Ut ut ut consequatur exercitationem.', '2018-11-13 02:10:27', '2018-11-13 02:10:27'),
(18, 'Quo dignissimos aliquid dolore natus amet in ea.', 'Numquam facilis sit quod molestiae molestias voluptatem. Sunt quis sint ad officiis dolores cumque autem. Quia sit repellendus et eum sequi omnis nobis. Dolore necessitatibus ut quo et.', '2018-11-13 02:10:27', '2018-11-13 02:10:27'),
(19, 'Praesentium repellat temporibus rerum repellat.', 'Voluptatem quam repellat consectetur facilis voluptatum aut. Inventore aliquam dolorem aut et. Unde odit voluptatibus officiis.', '2018-11-13 02:10:27', '2018-11-13 02:10:27'),
(20, 'Voluptatem temporibus impedit dolorem dolore quisquam.', 'Explicabo quos inventore maiores expedita. Molestiae quasi praesentium totam qui deserunt qui repellat ullam. Quae a ut quisquam consectetur ullam.', '2018-11-13 02:10:27', '2018-11-13 02:10:27'),
(21, 'Id praesentium cupiditate dolorem atque dolorem aut sed.', 'Mollitia assumenda eos aspernatur quisquam expedita ut ipsam. Voluptas ipsum sint at voluptates. Maxime repudiandae quo error. Aut tempora pariatur quibusdam voluptatem dolores.', '2018-11-13 02:10:27', '2018-11-13 02:10:27'),
(22, 'Sit exercitationem officiis temporibus et aliquid in sit esse.', 'Enim ea neque quis rem consequatur quisquam beatae. Reprehenderit repudiandae aut dolore animi. Cumque excepturi rerum iure et culpa deleniti. Doloribus dolores est minima tempora.', '2018-11-13 02:10:27', '2018-11-13 02:10:27'),
(23, 'Eaque blanditiis iusto animi minus iure inventore.', 'Adipisci nihil modi aliquam minus. Sunt in ipsum consequatur et esse qui aspernatur. Quo consequatur deleniti excepturi possimus et ipsa quia. Natus sunt voluptatem repudiandae atque sed dicta ullam.', '2018-11-13 02:10:27', '2018-11-13 02:10:27'),
(24, 'Debitis vero totam quo debitis rerum molestiae.', 'Quia dolor distinctio qui ipsam. Et fuga rerum debitis facilis et nisi. Ad accusantium sit natus dolor debitis. Iure magni eligendi nisi voluptas mollitia. Odio adipisci voluptas tenetur est dicta et.', '2018-11-13 02:10:28', '2018-11-13 02:10:28'),
(25, 'Temporibus dolores eum dolores alias sapiente sunt alias.', 'Tempora odio id facere nobis. Sed ad eaque animi et. Repellendus placeat sunt repellendus. Repellat dolores est molestiae dolore cupiditate reprehenderit sit. Velit non quo soluta numquam autem et architecto.', '2018-11-13 02:10:28', '2018-11-13 02:10:28'),
(62, '123123', 'asdsadsa', '2018-11-13 03:49:40', '2018-11-13 03:49:40'),
(63, '2222', '09564132', NULL, NULL),
(64, '2222', '09564132', NULL, NULL),
(65, '2222', '09564132', '2018-11-13 03:54:22', '2018-11-13 03:54:22'),
(66, '2222', '09564132', '2018-11-13 03:55:05', '2018-11-13 03:55:05'),
(67, '2222', '09564132', '2018-11-13 03:55:48', '2018-11-13 03:55:48'),
(68, '2222', '09564132', '2018-11-13 03:58:44', '2018-11-13 03:58:44'),
(69, '2222', '09564132', '2018-11-13 05:05:22', '2018-11-13 05:05:22'),
(70, '10000', '135985163156816', '2018-11-13 05:05:39', '2018-11-13 05:05:39');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `customer_id` int(150) NOT NULL COMMENT '編號',
  `card_id` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '卡號',
  `lastname` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '姓',
  `firstname` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '名',
  `birthday` date NOT NULL COMMENT '生日',
  `telephone` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '手機',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'Email',
  `address` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '地址',
  `company` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '公司行號名稱',
  `established` date NOT NULL COMMENT '公司成立時間',
  `business_attribute` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '公司業務屬性',
  `note` varchar(300) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '備註',
  `consumption_times` int(110) DEFAULT NULL COMMENT '消費次數',
  `cost_total` int(200) DEFAULT NULL COMMENT '消費總額',
  `updated_at` datetime DEFAULT NULL COMMENT '修改時間',
  `created_at` datetime NOT NULL COMMENT '新增時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customer_id`, `card_id`, `lastname`, `firstname`, `birthday`, `telephone`, `email`, `address`, `company`, `established`, `business_attribute`, `note`, `consumption_times`, `cost_total`, `updated_at`, `created_at`) VALUES
(1, 'AA123456789', 'chang', 'james', '2018-08-23', '09513274893', '567@yahoo.com.tw', '高雄市567號', '567', '2017-11-16', '網路資訊', NULL, 0, 0, NULL, '2018-11-08 00:00:00'),
(2, 'AA111111111', '韓', '國魚', '1977-06-26', '09123456789', '123@yahoo.com.tw', '高雄市韓國路', '567', '2010-05-02', '科技', NULL, 1, 1000, NULL, '2018-11-08 03:58:26'),
(3, 'AA222222222', '陳', '奇麥', '1980-10-29', '0924682597', '123@yahoo.com.tw', '高雄市奇麥路', '567', '2010-05-02', '金融保險', NULL, 0, 0, NULL, '2018-11-08 04:08:01'),
(4, 'AA333333333', '阿', '貓', '1980-10-29', '095675289', '567@yahoo.com.tw', '高雄市更新路', '567', '2010-05-02', '教育', NULL, 0, 0, '2018-11-08 08:26:13', '2018-11-08 05:13:32'),
(7, 'QQ987987987', '花', '媽', '1981-06-18', '09115677892', '9851247@yahoo.com.tw', '高雄市花媽路8路', '567', '2010-05-02', '其他', '', 0, 0, NULL, '2018-11-09 09:08:02'),
(9, 'DD168463135', '福', '來爹', '1968-01-15', '09567472358', '567@yahoo.com.tw', '台南市成功路', '567', '2010-05-02', '教育', '', 0, 0, '2018-11-12 07:28:00', '2018-11-12 06:40:38'),
(14, 'AM9841123416e1', '李', '史丹', '1959-06-29', '09248145256854', '567@yahoo.com.tw', '美國', '567', '2010-05-02', '科技', '', 0, 0, '2018-11-13 08:53:28', '2018-11-13 06:09:02'),
(16, 'DFSGS19681615', '浩', '克', '1982-04-17', '093589614569', '567@yahoo.com.tw', '美國', '567', '2010-05-02', '其他', '', 0, 0, '2018-11-13 09:15:46', '2018-11-13 09:15:46'),
(18, 'DSFDSF641643', '李', '史丹', '1959-06-29', '092545625', '567@yahoo.com.tw', '美國', '567', '2010-05-02', '金融保險', '', 0, 0, '2018-11-14 05:35:50', '2018-11-14 05:35:50'),
(22, 'QWEWSA12389da9s84', '5', '67', '1567-05-06', '0956788888', '567888@gmail.com', '高雄市發發路888號', '567', '2010-05-02', '網路資訊', '', 8, 56700000, '2018-11-15 09:27:04', '2018-11-15 09:27:04');

-- --------------------------------------------------------

--
-- Table structure for table `detail`
--

CREATE TABLE `detail` (
  `detail_id` int(100) NOT NULL COMMENT '編號',
  `orderlist_id` int(100) NOT NULL COMMENT '對應orderlist',
  `item` varchar(250) NOT NULL COMMENT '項目',
  `price` varchar(300) NOT NULL COMMENT '價格',
  `quantity` varchar(200) NOT NULL COMMENT '數量',
  `subtotal` int(50) NOT NULL COMMENT '小計'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `detail`
--

INSERT INTO `detail` (`detail_id`, `orderlist_id`, `item`, `price`, `quantity`, `subtotal`) VALUES
(1, 1, 'A005/B032/B073/C015', '50000/660000/260000/120000', '1/2/1/3', 1990000),
(2, 3, 'A005', '50000', '2', 100000),
(3, 5, 'B073', '260000', '1', 260000),
(4, 6, 'C015', '120000', '3', 360000),
(5, 2, 'B032/C015', '660000/120000', '1/1', 780000);

-- --------------------------------------------------------

--
-- Table structure for table `first_sort`
--

CREATE TABLE `first_sort` (
  `first_sort_id` int(50) NOT NULL COMMENT '編號',
  `simplified_chinese` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '簡中',
  `english` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '英文',
  `vietnamese` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '越文'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `first_sort`
--

INSERT INTO `first_sort` (`first_sort_id`, `simplified_chinese`, `english`, `vietnamese`) VALUES
(1, '粵式點心', 'DIM SUM', 'DIM SUM'),
(2, '单点美馔', 'ALACARTE MENU', 'THỰC ĐƠN'),
(3, '优质套餐', 'SET MENU', 'SET MENU'),
(4, '酒水饮品', 'DRINK LIST', 'ĐỒ UỐNG'),
(5, '主廚推薦', 'DISHES OF MONTH', 'MÓN ĂN ĐỀ XUẤT');

-- --------------------------------------------------------

--
-- Table structure for table `friend`
--

CREATE TABLE `friend` (
  `friend_id` int(100) NOT NULL COMMENT '編號',
  `customer_id` int(100) NOT NULL COMMENT '對應customer',
  `relationship` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '關係',
  `lastname` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '姓',
  `firstname` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '名',
  `birthday` date NOT NULL COMMENT '生日',
  `telephone` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '手機',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'Email',
  `address` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '地址',
  `be_customer` varchar(5) NOT NULL COMMENT '是否會員:Y/N',
  `card_id` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '會員卡號',
  `updated_at` datetime DEFAULT NULL COMMENT '修改時間',
  `created_at` datetime NOT NULL COMMENT '新增時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `friend`
--

INSERT INTO `friend` (`friend_id`, `customer_id`, `relationship`, `lastname`, `firstname`, `birthday`, `telephone`, `email`, `address`, `be_customer`, `card_id`, `updated_at`, `created_at`) VALUES
(1, 1, '同事', '陳', '奕安', '2018-08-22', '091357913579', '13579@yahoo.com.tw', '忠孝東路9號', 'n', '', NULL, '0000-00-00 00:00:00'),
(2, 1, '朋友', '許', '佑明', '2018-08-14', '0924682468', '2468@gmail.com', '林森北路7號', 'n', '', NULL, '0000-00-00 00:00:00'),
(5, 3, '家人', '芋', '頭', '1971-02-14', '09671258365', 'uuuuuuu@yahoo.com.tw', '中國', 'y', 'CCC5165123', '2018-11-09 02:19:14', '2018-11-08 09:18:17'),
(6, 3, '其他', '蕃', '薯', '1966-07-11', '0908526742', 'bbbbb@yahoo.com.tw', '地球村', 'n', '', NULL, '2018-11-08 09:20:01'),
(10, 1, '家人', '地', '瓜', '1951-07-30', '0913651531', 'rewr@yahoo.com.tw', '中國', 'n', '', NULL, '2018-11-12 07:35:30'),
(12, 14, '家人', '金', '剛郎', '1951-07-30', '0913651531', 'jhtds5551@yahoo.com.tw', '美國', 'n', '', NULL, '2018-11-13 07:12:28'),
(13, 14, '同事', '美國', '隊長', '1951-08-11', '094713287456', 'asd49856ab98nj@yahoo.com.tw', '美國', 'n', '', '2018-11-13 07:50:22', '2018-11-13 07:13:29'),
(14, 14, '同事', '剛', '鐵人', '1951-07-30', '0952543268542', 'gew66s26s9e7@yahoo.com.tw', '美國', 'y', '4354345487971587', '2018-11-13 07:55:09', '2018-11-13 07:13:29');

-- --------------------------------------------------------

--
-- Table structure for table `management`
--

CREATE TABLE `management` (
  `id` int(100) NOT NULL COMMENT '編號',
  `user_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '帳號',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '密碼',
  `groups` int(10) NOT NULL,
  `api_token` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `management`
--

INSERT INTO `management` (`id`, `user_id`, `password`, `groups`, `api_token`) VALUES
(1, 'admin', '567567', 0, NULL),
(2, 'abc123', '123456', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(250) NOT NULL,
  `meal_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '品項編號',
  `first_sort_id` varchar(50) NOT NULL COMMENT '對應first_sort',
  `second_sort_id` varchar(50) DEFAULT NULL COMMENT '對應second_sort',
  `third_sort_id` varchar(50) DEFAULT NULL COMMENT '對應third_sort',
  `add_purchase` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '加購項目',
  `simplified_chinese` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '簡中',
  `english` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '英文',
  `vietnamese` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '越文',
  `pic` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '圖檔',
  `price` varchar(150) NOT NULL COMMENT '價格',
  `tax` varchar(50) NOT NULL COMMENT '稅',
  `quantity` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '是否份量選擇:Y/N',
  `quantity_ch` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '份量簡中',
  `quantity_en` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '份量英文',
  `quantity_vi` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '份量越文'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `meal_id`, `first_sort_id`, `second_sort_id`, `third_sort_id`, `add_purchase`, `simplified_chinese`, `english`, `vietnamese`, `pic`, `price`, `tax`, `quantity`, `quantity_ch`, `quantity_en`, `quantity_vi`) VALUES
(1, 'A001', '1', '1', NULL, NULL, '瑶柱灌汤饺', 'Steamed Dried Scallop Dumpling Served In Soup', 'Súp bánh xếp sò điệp', '/cms/public/pic/A002.jpg', '88000', '10%', 'Y', '小/中/大', 'Small/Medium/Large', 'NHỎ/TRUNG/LỚN'),
(2, 'A002', '1', '1', NULL, NULL, '水晶鲜虾饺', 'Steamed Shrimp Dumpling \"Ha Gow\"', 'Há cảo thủy tinh', '/cms/public/pic/A002.jpg', '68000', '10%', 'N', NULL, NULL, NULL),
(3, 'A003', '1', '1', NULL, NULL, '瑶柱带子饺', 'Steamed Scallop Dumpling With Dried Scallop', 'Há cảo sò điệp', '/cms/public/pic/A002.jpg', '68000', '10%', 'N', NULL, NULL, NULL),
(4, 'A004', '1', '1', NULL, NULL, '香菇蒸烧卖', 'Steamed Pork Dumpling With Mushroom \"Siew Mai\"', 'Xíu mại', '/cms/public/pic/A002.jpg', '68000', '10%', 'N', NULL, NULL, NULL),
(5, 'A005', '1', '1', NULL, NULL, '潮州蒸粉粿', 'Steamed Pork Dumpling In \'Toechew\' Style', 'Bánh xếp Triều Châu', '/cms/public/pic/A002.jpg', '50000', '10%', 'Y', '小/中/大', 'Small/Medium/Large', 'NHỎ/TRUNG/LỚN'),
(6, 'A008', '1', '1', NULL, NULL, '鼓汁蒸排骨', 'Steamed Spare Rib with Black Bean Sauce', 'Sườn non hấp tàu xì', '/cms/public/pic/A002.jpg', '58000', '10%', 'N', NULL, NULL, NULL),
(7, 'A009', '1', '1', NULL, NULL, '蚝皇鲜竹卷', 'Steamed Beabcurd Skin Roll With Oyster Sauce', 'Tàu hũ ky cuộn hấp dầu hào', '/cms/public/pic/A002.jpg', '58000', '10%', 'N', NULL, NULL, NULL),
(8, 'A010', '1', '1', NULL, NULL, '豉汁蒸凤爪', 'Steamed Chicken Feet With Black Bean Sauce', 'Chân gà hấp tàu xì', '/cms/public/pic/A002.jpg', '55000', '10%', 'Y', '小/中/大', 'Small/Medium/Large', 'NHỎ/TRUNG/LỚN'),
(9, 'A011', '1', '1', NULL, NULL, '上海小笼包', 'Steamed Shanghai Style Pork Dumpling \"Xiao Long Bao\"', 'Bánh bao Thượng Hải', '/cms/public/pic/A002.jpg', '58000', '10%', 'Y', '小/中/大', 'Small/Medium/Large', 'NHỎ/TRUNG/LỚN'),
(10, 'A012', '1', '1', NULL, NULL, 'XO酱煎肠粉', 'Sauteed Rice Roll With XO Sauce', 'Bánh cuốn xào tương XO', '/cms/public/pic/A002.jpg', '80000', '10%', 'N', NULL, NULL, NULL),
(11, 'B001', '2', '3', NULL, NULL, '五福临门打进盘', 'YU SHANG LOU Appetizer Five Combination', 'Ngũ phúc lâm môn khai vị', '/cms/public/pic/A002.jpg', '580000/290000', '10%', 'Y', NULL, NULL, 'Lớn / Nhỏ'),
(12, 'B002', '2', '3', NULL, NULL, '四川口水鸡', 'Spicy chicken in \' Sichuan \' style', 'Gà Tứ Xuyên', '/cms/public/pic/A002.jpg', '150000', '10%', 'N', NULL, NULL, NULL),
(13, 'B017', '2', '4', NULL, NULL, '御尚楼富贵猪', '\"Yu Shang Lou\" Roasted suckling pig stuffed with preserved meat fried rice', 'Heo phú quý Yu Shang Lou', '/cms/public/pic/A002.jpg', '1680000', '10%', 'N', NULL, NULL, NULL),
(14, 'B018', '2', '4', NULL, NULL, '鸿运乳猪全体', 'Roasted suckling pig', 'Heo sữa quay', '/cms/public/pic/A002.jpg', '150000/80000', '10%', 'Y', '只/半只', 'Whole/Half', 'Con/Nửa con'),
(15, 'B021', '2', '4', NULL, NULL, '明炉吊烧鸭', 'Roasted duck', 'Vịt quay ', '/cms/public/pic/A002.jpg', '680000/350000/200000', '10%', 'Y', '只/半只/小', 'Whole/Half/Small', 'Con/Nửa con/Phần nhỏ'),
(16, 'B023', '2', '4', NULL, NULL, '驰名贵妃鸡', 'Steamed chicken', 'Gà luộc quý phi', '/cms/public/pic/A002.jpg', '680000/350000/200000', '10%', 'Y', '只/半只/小', 'Whole/Half/Small', 'Con/Nửa con/Phần nhỏ'),
(17, 'B031', '2', '5', NULL, NULL, '石锅鱼翅', 'Braised superior shark\'s fin in hot stone', 'Súp vi cá tay cầm', '/cms/public/pic/A002.jpg', '660000', '10%', 'N', NULL, NULL, NULL),
(18, 'B032', '2', '5', NULL, NULL, '红烧鲍翅', 'Braised superior shark\' s fin with supreme sauce', 'Súp vi cá hồng xíu', '/cms/public/pic/A002.jpg', '660000', '10%', 'N', '小/中/大', 'Small/Medium/Large', 'NHỎ/TRUNG/LỚN'),
(19, 'B038', '2', '6', NULL, NULL, '八宝节瓜盅', 'Double-boiled melon with seafood soup', 'Súp trái bí tiềm hải sản', '/cms/public/pic/A002.jpg', '180000', '10%', 'N', NULL, NULL, NULL),
(20, 'B039', '2', '6', NULL, NULL, '羊肚菌炖鸡汤', 'Double-boiled chicken soup with morel Mushroom', 'Canh bong bóng dê tiềm gà', '/cms/public/pic/A002.jpg', '200000', '10%', 'N', NULL, NULL, NULL),
(21, 'B048', '2', '7', NULL, NULL, '生捞鲜鲍片', 'Special Sliced Raw Abalone', 'Bào ngư miếng trộn khô', '/cms/public/pic/A002.jpg', '820000', '10%', 'N', NULL, NULL, NULL),
(22, 'B054', '2', '7', NULL, NULL, '蚝油鲍片扒时蔬', 'Braised sliced abalone with oyster sauce & seasonal Vegetable', 'Bào ngư miếng hầm dầu hào', '/cms/public/pic/A002.jpg', '520000', '10%', 'N', NULL, NULL, NULL),
(23, 'B060', '2', '8', NULL, NULL, '黑松露油赛螃带子', 'Stir-fried fresh scallop with egg white & black truffle oil', 'Sốt trứng xào sò điệp tươi', '/cms/public/pic/A002.jpg', '180000', '10%', 'N', NULL, NULL, NULL),
(24, 'B064', '2', '8', NULL, NULL, '富贵海鲜石榴球', 'Steamed diced seafood wrapped with egg white skin', 'Hải sản viên hấp', '/cms/public/pic/A002.jpg', '220000', '10%', 'N', NULL, NULL, NULL),
(25, 'B073', '2', '9', NULL, NULL, '黑椒炒牛柳粒', 'Wok-fried Tenderlion beef cube with black pepper sauce', 'Bò lúc lắc xào sốt tiêu đen', '/cms/public/pic/A002.jpg', '260000', '10%', 'N', NULL, NULL, NULL),
(26, 'B078', '2', '9', NULL, NULL, '剁椒蒸田鸡', 'Steamed frog with chopped chili', 'Ếch hấp sa tế', '/cms/public/pic/A002.jpg', '180000', '10%', 'N', NULL, NULL, NULL),
(27, 'B086', '2', '10', NULL, NULL, '碧绿炒带子', 'Sauteed scallop with broccoli', 'Bông cải xanh xào sò điệp tươi', '/cms/public/pic/A002.jpg', '320000', '10%', 'N', NULL, NULL, NULL),
(28, 'B089', '2', '10', NULL, NULL, '崧菇肉松扒豆腐', 'Braised home made bean curd with minced pork & mushroom', 'Đậu hũ thịt bằm và nấm sốt đặc biệt', '/cms/public/pic/A002.jpg', '250000', '10%', 'N', NULL, NULL, NULL),
(29, 'C011', '4', '16', '2', NULL, '莫希多', 'Mojito', 'Mojito', '/cms/public/pic/A002.jpg', '80000', '10%', 'N', NULL, NULL, NULL),
(30, 'C015', '4', '16', '2', NULL, '僵尸', 'Zombie', 'Zombie', '/cms/public/pic/A002.jpg', '120000', '10%', 'N', NULL, NULL, NULL),
(31, 'C071', '4', '18', '8', NULL, '慕尼黑皇家宫廷小麦白啤酒', 'Hofbraeu Muenchner Weisse', 'Hofbraeu Muenchner Weisse', '/cms/public/pic/A002.jpg', '148000', '10%', 'N', NULL, NULL, NULL),
(32, 'C073', '4', '18', '9', NULL, '德国熊窖藏啤酒', 'Harboe Bear Strong Lager', 'Harboe Bear Strong Lager', '/cms/public/pic/A002.jpg', '78000', '10%', 'N', NULL, NULL, NULL),
(33, 'C076', '4', '18', '10', NULL, '巴顿宝精酿啤酒', 'Paderborner Pilger', 'Paderborner Pilger', '/cms/public/pic/A002.jpg', '60000', '10%', 'N', NULL, NULL, NULL),
(34, 'C078', '4', '18', '11', NULL, '西贡333啤酒', 'Bia 333', 'Bia 333', '/cms/public/pic/A002.jpg', '35000', '10%', 'N', NULL, NULL, NULL),
(35, 'E001', '5', NULL, NULL, NULL, '姜葱牛柏叶', 'Steamed Beef Omasum With Ginger & Scallion ', 'Lá Sách Bò Hành Gừng', '/cms/public/pic/A002.jpg', '68000', '10%', 'N', NULL, NULL, NULL),
(36, 'E002', '5', NULL, NULL, NULL, '上素冬菇包', 'Steamed Vegetarian Mushroom Bun', 'Bánh Bao Đông Cô', '/cms/public/pic/A002.jpg', '58000', '10%', 'N', NULL, NULL, NULL),
(37, 'E003', '5', NULL, NULL, NULL, '冬菇鸡球烧卖', 'Steamed Chicken Dumpling With Mushroom \"Sieu Mai\" ', 'Xíu Mại Gà Đông Cô', '/cms/public/pic/A002.jpg', '68000', '10%', 'N', NULL, NULL, NULL),
(38, 'E004', '5', NULL, NULL, NULL, '香脆韭菜角', 'Pan-Fried Crispy Chives Pancake', 'Bánh Xếp Hẹ Chiên Giòn', '/cms/public/pic/A002.jpg', '58000', '10%', 'N', NULL, NULL, NULL),
(39, 'E005', '5', NULL, NULL, NULL, '萝卜丝烧饼', 'Baked Radish Pastry ', 'Bánh Chiên Củ Cải', '/cms/public/pic/A002.jpg', '58000', '10%', 'N', NULL, NULL, NULL),
(40, 'E006', '5', NULL, NULL, NULL, '流沙芝麻球', 'Deep-Fried Lava Glutinous Sesame Ball', 'Bánh Mè Kim Sa', '/cms/public/pic/A002.jpg', '55000', '10%', 'N', NULL, NULL, NULL),
(41, 'E007', '5', NULL, NULL, NULL, '自家四川泡菜', 'Home-Made Chilled \"Sichuan\" Style Pickle (Kimchi) ', 'Kim Chi Tứ Xuyên', '/cms/public/pic/A002.jpg', '80000', '10%', 'N', NULL, NULL, NULL),
(42, 'E008', '5', NULL, NULL, NULL, '宫廷蛋黄鸭卷', 'Chilled Marinated Sliced Duck Roll Stuffed With Egg York ', 'Thịt Vịt Cuộn Trứng Chiên', '/cms/public/pic/A002.jpg', '160000', '10%', 'N', NULL, NULL, NULL),
(43, 'E009', '5', NULL, NULL, NULL, '秘制红酒靓瓜脯', 'Chef Secret Recipe Chilled Melon With Red Wine Sauce', 'Đặc Chế Quả Bí Đao Ngâm Rượu Vang', '/cms/public/pic/A002.jpg', '8000', '10%', 'N', NULL, NULL, NULL),
(44, 'E010', '5', NULL, NULL, NULL, '红烧大元蹄', 'Braised Pig Trtter In Brown Sauce ', 'Giò Heo Hồng Xíu', '/cms/public/pic/A002.jpg', '560000', '10%', 'N', NULL, NULL, NULL),
(45, 'E011', '5', NULL, NULL, NULL, '鲜什果沙律靓虾', 'Fresh Shrimp With Assorted Fruit & Mayonaise Sause', 'Salad Trái Cây Cuộn Tôm', '/cms/public/pic/A002.jpg', '320000', '10%', 'N', NULL, NULL, NULL),
(46, 'E012', '5', NULL, NULL, NULL, '酥炸荔子盒', 'Deep Fried Fresh Scallop Coated With Mashed Taro Paste ', 'Khoai Môn Sò Điệp Chiên Giòn', '/cms/public/pic/A002.jpg', '320000', '10%', 'N', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer`
--

CREATE TABLE `oc_customer` (
  `id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `pw` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `cart` text,
  `wishlist` text,
  `newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `address_id` int(11) NOT NULL DEFAULT '0',
  `custom_field` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `safe` tinyint(1) NOT NULL,
  `token` text NOT NULL,
  `code` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_customer`
--

INSERT INTO `oc_customer` (`id`, `customer_group_id`, `store_id`, `language_id`, `firstname`, `lastname`, `email`, `telephone`, `fax`, `pw`, `salt`, `cart`, `wishlist`, `newsletter`, `address_id`, `custom_field`, `ip`, `status`, `safe`, `token`, `code`, `date_added`) VALUES
(1, 1, 0, 2, 'ＴＨＳＤＦ', 'dtgjqpj', 'ijfpq@gmail.com', '0912345678', '', '91e1ab7237d8754eec2192221558028a45a4d638', 'whUwidDUO', NULL, NULL, 0, 1, '[]', '59.125.153.173', 1, 0, '', '', '2018-09-17 08:46:20'),
(2, 1, 0, 3, 'janet', 'lai', '123@yahoo.com', '123456789', '', '8ea7695e5f18d18ab1b10d8a9d26ed29a499ecfe', '0khRmkavk', NULL, NULL, 0, 0, '', '59.125.153.173', 1, 0, '', '', '2018-09-18 10:14:02'),
(3, 1, 0, 3, 'aa', 'aa', 'aa@aa.aa', '0912345678', '', '093694db0bcf0f32a90c2f005c6118db68083f59', 'bC85bT44W', NULL, NULL, 0, 2, '', '59.125.153.173', 1, 0, '', '', '2018-09-18 14:00:55'),
(4, 1, 0, 2, 'testtest', 'lai', 'ijfpq1234564@gmail.com', '123456879', '', 'c017b0bbd38796dd7d3aa7d15c9ef7463faa9e52', 'JCRHcHsmc', NULL, NULL, 0, 0, '', '59.125.153.173', 1, 0, '', '', '2018-09-18 16:39:30');

-- --------------------------------------------------------

--
-- Table structure for table `orderlist`
--

CREATE TABLE `orderlist` (
  `orderlist_id` int(200) NOT NULL COMMENT '編號',
  `customer_id` int(200) NOT NULL COMMENT '對應customer',
  `add_time` datetime NOT NULL COMMENT '訂單時間',
  `department` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '消費店家:A店,B店',
  `order_way` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '點餐方式:現場訂單',
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '類型:內用,外帶',
  `amount` int(100) NOT NULL COMMENT '消費金額',
  `detail_id` int(100) NOT NULL COMMENT '餐點明細',
  `tax` int(50) NOT NULL COMMENT '稅金',
  `total` int(150) NOT NULL COMMENT '總計',
  `note` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '備註',
  `updated_at` datetime DEFAULT NULL COMMENT '修改時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orderlist`
--

INSERT INTO `orderlist` (`orderlist_id`, `customer_id`, `add_time`, `department`, `order_way`, `type`, `amount`, `detail_id`, `tax`, `total`, `note`, `updated_at`) VALUES
(1, 2, '2018-10-17 07:32:29', 'A店', '現場訂單', '內用', 1000, 1, 150, 1150, '測試內用', NULL),
(2, 6, '2018-10-31 20:29:44', 'B店', '現場訂單', '外帶', 20000, 2, 2000, 22000, '測試外帶', NULL),
(3, 12, '2017-09-27 17:29:44', 'A店', '現場訂單', '內用', 20000, 2, 2000, 22000, NULL, NULL),
(5, 13, '2017-09-27 17:29:44', 'A店', '現場訂單', '內用', 20000, 2, 2000, 22000, NULL, NULL),
(6, 14, '2017-02-14 15:36:59', 'B店', '現場訂單', '外帶', 66600, 3, 66600, 1322200, '更新測試', '2018-11-14 08:20:11'),
(13, 55, '2017-09-27 17:29:44', 'A店', '現場訂單', '內用', 20000, 2, 2000, 22000, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `second_sort`
--

CREATE TABLE `second_sort` (
  `second_sort_id` int(50) NOT NULL COMMENT '編號',
  `first_sort_id` int(50) NOT NULL COMMENT '對應first_sort',
  `simplified_chinese` varchar(100) NOT NULL COMMENT '簡中',
  `english` varchar(100) NOT NULL COMMENT '英文',
  `vietnamese` varchar(100) NOT NULL COMMENT '越文'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `second_sort`
--

INSERT INTO `second_sort` (`second_sort_id`, `first_sort_id`, `simplified_chinese`, `english`, `vietnamese`) VALUES
(1, 1, '粵式點心', 'Dimsum', 'Điểm tâm'),
(2, 1, '汤麺，粥', 'Soup Noodle - Congee', 'Mì nước, cháo'),
(3, 2, '餐前小食', 'Appetizers', 'Món khai vị nguội'),
(4, 2, '烧味', 'Barbecured', 'Món quay'),
(5, 2, '鱼翅类', 'Shark\'s Fin', 'Vi cá'),
(6, 2, '汤类', 'Soup', 'Canh'),
(7, 2, '鲍鱼海味类', 'Abalone & Dried Seafood', 'Bào ngư hải vị'),
(8, 2, '海产类', 'Seafood', 'Hải sản tươi sống'),
(9, 2, '肉类', 'Meat', 'Các loại thịt'),
(10, 2, '蔬菜/ 豆腐类', 'Vegetable / Beancurd', 'Các loại rau'),
(11, 2, '饭/面类', 'Rice / Noodle', 'Cơm, mì'),
(12, 2, '活鱼类', 'Live Fish', 'Các loài cá'),
(13, 2, '贝类', 'Clam', 'Các loại ốc'),
(14, 2, '生猛龙虾/生虾类', 'Live Lobster / Prawn', 'Tôm hùm/ Các loại tôm tươi'),
(15, 4, '精选甜品', 'Dessert', 'Chè và tráng miệng'),
(16, 4, '鸡尾酒', 'Cocktails', 'Cocktails'),
(17, 4, '软性饮品', 'Dink List', 'Dink List'),
(18, 4, '啤酒', 'Beer', 'Bia'),
(19, 4, '顶级苹果酒', 'PREMIUM CIDER', 'PREMIUM CIDER'),
(20, 4, '单杯酒', 'WINE BY GLASS', 'WINE BY GLASS'),
(21, 4, '红酒', 'RED WINE', 'RED WINE'),
(22, 4, '白酒', 'WHITE WINE', 'WHITE WINE'),
(23, 4, '粉红酒', 'ROSE WINE', 'ROSE WINE'),
(24, 4, '气泡香槟', 'CHAMPAGNE & SPARKLING', 'CHAMPAGNE & SPARKLING'),
(25, 4, '烧酒', 'SOJU', 'SOJU'),
(26, 4, '伏特加', 'VODKA', 'VODKA'),
(27, 4, '威士忌', 'WHISKY', 'WHISKY'),
(28, 4, '干邑白兰地', 'COGNAC', 'COGNAC'),
(29, 4, '中式烈酒', 'CHINESE WHISKY', 'CHINESE WHISKY');

-- --------------------------------------------------------

--
-- Table structure for table `survey`
--

CREATE TABLE `survey` (
  `survey_id` int(100) NOT NULL COMMENT '編號',
  `orderlist_id` int(100) NOT NULL COMMENT '對應order',
  `visits_times` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '來店次數',
  `pipeline` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '透過管道',
  `purpose` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '目的',
  `people` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '用餐人數',
  `attitude` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '態度滿意度',
  `effectiveness` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '速度滿意度',
  `introduction` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '介紹滿意度',
  `note` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '備註',
  `updated_at` datetime DEFAULT NULL COMMENT '修改時間',
  `created_at` datetime NOT NULL COMMENT '新增時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `survey`
--

INSERT INTO `survey` (`survey_id`, `orderlist_id`, `visits_times`, `pipeline`, `purpose`, `people`, `attitude`, `effectiveness`, `introduction`, `note`, `updated_at`, `created_at`) VALUES
(1, 1, '1次', 'FB廣告', '享用美食', '1人', '滿意', '普通', '滿意', '', NULL, '2018-11-08 00:00:00'),
(2, 2, '1次以上', '網路通路', '朋友聚會', '2人', '滿意', '滿意', '滿意', '', NULL, '2018-11-09 00:00:00'),
(5, 5, '1次以上', 'FB廣告', '家族聚會', '10人以上', '普通', '滿意', '滿意', '', NULL, '2018-10-25 00:00:00'),
(6, 6, '1次', '朋友介紹', '招待客戶', '4人', '滿意', '滿意', '普通', '', NULL, '2018-10-15 00:00:00'),
(10, 15, '1次以上', '朋友介紹', '招待客戶', '4人', '普通', '普通', '普通', '更新測試', '2018-11-09 07:32:16', '2018-10-15 00:00:00'),
(15, 66, '1次', '網路通路', '招待客戶', '2人', '普通', '普通', '普通', '更新測試', '2018-11-15 10:35:36', '2018-11-15 10:31:30'),
(17, 66, '1次以上', '朋友介紹', '家族聚會', '10人以上', '普通', '滿意', '普通', '新增測試', NULL, '2018-11-15 11:22:15'),
(18, 66, '1次以上', '朋友介紹', '家族聚會', '10人以上', '普通', '滿意', '普通', '新增測試', NULL, '2018-11-15 11:22:33');

-- --------------------------------------------------------

--
-- Table structure for table `third_sort`
--

CREATE TABLE `third_sort` (
  `third_sort_id` int(50) NOT NULL COMMENT '編號',
  `second_sort_id` int(50) NOT NULL COMMENT '對應second_sort',
  `simplified_chinese` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '簡中',
  `english` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '英文',
  `vietnamese` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '越文'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `third_sort`
--

INSERT INTO `third_sort` (`third_sort_id`, `second_sort_id`, `simplified_chinese`, `english`, `vietnamese`) VALUES
(1, 15, '精选甜品', 'Dessert', 'Chè và tráng miệng'),
(2, 16, '兰姆酒鸡尾酒', 'Rum Cocktails', 'Rum Cocktails'),
(3, 16, '琴酒鸡尾酒', 'Gin Cocktails', 'Gin Cocktails'),
(4, 16, '龙舌兰鸡尾酒', 'Tequila Cocktails', 'Tequila Cocktails'),
(5, 16, '伏特加鸡尾酒', 'Vodka Cocktails', 'Vodka Cocktails'),
(6, 16, '特调鸡尾酒', 'Other Cocktails', 'Other Cocktails'),
(7, 17, '软性饮品', 'Dink List', 'Dink List'),
(8, 18, '慕尼黑皇家', 'HB BEER', 'HB BEER'),
(9, 18, '德国熊', 'BEER BEAR', 'BEER BEAR'),
(10, 18, '巴顿宝', 'PADERBORNER BEER', 'PADERBORNER BEER'),
(11, 18, '各国啤酒', 'NATIONAL BEER', 'NATIONAL BEER'),
(12, 19, '布伦蒂斯', 'BRUNTYS', 'BRUNTYS'),
(13, 20, '智利产区', 'CHILE', 'CHILE'),
(14, 21, '法国产区', 'FRANCE', 'FRANCE'),
(15, 21, '澳洲产区', 'AUSTRALIAN', 'AUSTRALIAN'),
(16, 21, '智利产区', 'CHILE', 'CHILE'),
(17, 21, '义大利产区', 'ITALY', 'ITALY'),
(18, 21, '美国产区', 'AMERICAN', 'AMERICAN'),
(19, 22, '法国产区', 'FRANCE', 'FRANCE'),
(20, 22, '澳洲产区', 'AUSTRALIAN', 'AUSTRALIAN'),
(21, 22, '智利产区', 'CHILE', 'CHILE'),
(22, 22, '美国产区', 'AMERICAN', 'AMERICAN'),
(23, 23, '法国产区', 'FRANCE', 'FRANCE'),
(24, 24, '气泡香槟', 'CHAMPAGNE & SPARKLING', 'CHAMPAGNE & SPARKLING'),
(25, 25, '韩国烧酒', 'SOJU', 'SOJU'),
(26, 26, '伏特加', 'VODKA', 'VODKA'),
(27, 27, '威士忌', 'JOHNNIE WALKER', 'JOHNNIE WALKER'),
(28, 27, '起瓦士', 'CHIVAS', 'CHIVAS'),
(29, 27, '百龄坛', 'BALLANTINE', 'BALLANTINE'),
(30, 27, '麦卡伦', 'MACALLAN', 'MACALLAN'),
(31, 28, '人头马', 'REMY MARTIN', 'REMY MARTIN'),
(32, 28, '马爹利', 'MARTELL', 'MARTELL'),
(33, 28, '轩尼诗', 'HENNESSY', 'HENNESSY'),
(34, 29, '中式烈酒', 'CHINESE WHISKY', 'CHINESE WHISKY');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `api_token` varchar(100) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `api_token`, `updated_at`, `created_at`) VALUES
(13, '567admin', 'admin', '$2y$10$MVi7Rf46bKYZ6H/IbxJx0uo.J78GIqYk5IEC9SjzrhL7M7pARFK7e', 'DHL678hdNF4yQwv7twvBTtkR6apAnCGE4aMCyvZvSv0QuhTBFC8dyd6MBCLI', '2018-11-20 09:22:02', '2018-11-14 03:11:34'),
(14, '567admin2', 'admin2', '$2y$10$Rr9GlwePZO4bAUJO3M0HZu0gaFa8iK5ZX.Oc2Yf59cMZZ4OiZuH8u', '89Zc0c6xe0nE4wDJdwmEkPU8y9BJ32UxxPfsAa6NLOyfTHIi7Tl9tGFrOdaV', '2018-11-15 01:08:18', '2018-11-14 03:16:54');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `detail`
--
ALTER TABLE `detail`
  ADD PRIMARY KEY (`detail_id`);

--
-- Indexes for table `first_sort`
--
ALTER TABLE `first_sort`
  ADD PRIMARY KEY (`first_sort_id`);

--
-- Indexes for table `friend`
--
ALTER TABLE `friend`
  ADD PRIMARY KEY (`friend_id`);

--
-- Indexes for table `management`
--
ALTER TABLE `management`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_customer`
--
ALTER TABLE `oc_customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderlist`
--
ALTER TABLE `orderlist`
  ADD PRIMARY KEY (`orderlist_id`);

--
-- Indexes for table `second_sort`
--
ALTER TABLE `second_sort`
  ADD PRIMARY KEY (`second_sort_id`);

--
-- Indexes for table `survey`
--
ALTER TABLE `survey`
  ADD PRIMARY KEY (`survey_id`);

--
-- Indexes for table `third_sort`
--
ALTER TABLE `third_sort`
  ADD PRIMARY KEY (`third_sort_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `customer_id` int(150) NOT NULL AUTO_INCREMENT COMMENT '編號', AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `detail`
--
ALTER TABLE `detail`
  MODIFY `detail_id` int(100) NOT NULL AUTO_INCREMENT COMMENT '編號', AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `first_sort`
--
ALTER TABLE `first_sort`
  MODIFY `first_sort_id` int(50) NOT NULL AUTO_INCREMENT COMMENT '編號', AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `friend`
--
ALTER TABLE `friend`
  MODIFY `friend_id` int(100) NOT NULL AUTO_INCREMENT COMMENT '編號', AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `management`
--
ALTER TABLE `management`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT COMMENT '編號', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `oc_customer`
--
ALTER TABLE `oc_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `orderlist`
--
ALTER TABLE `orderlist`
  MODIFY `orderlist_id` int(200) NOT NULL AUTO_INCREMENT COMMENT '編號', AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `second_sort`
--
ALTER TABLE `second_sort`
  MODIFY `second_sort_id` int(50) NOT NULL AUTO_INCREMENT COMMENT '編號', AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `survey`
--
ALTER TABLE `survey`
  MODIFY `survey_id` int(100) NOT NULL AUTO_INCREMENT COMMENT '編號', AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `third_sort`
--
ALTER TABLE `third_sort`
  MODIFY `third_sort_id` int(50) NOT NULL AUTO_INCREMENT COMMENT '編號', AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
