<?php

use Illuminate\Http\Request;
use App\Article;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::group(['middleware' => 'cors'],function(){

	//隐式
	Route::group(['middleware' => 'auth:api'], function() {
		Route::get('articles', 'ArticleController@index');
		Route::get('articles/{article}', 'ArticleController@show');
		Route::post('articles', 'ArticleController@store');
		Route::put('articles', 'ArticleController@update');
		Route::delete('articles/{article}', 'ArticleController@delete');
	});

	//member_api
	Route::group(['middleware' => 'auth:api'], function() {
		// Route::get('memberapis', 'MemberapiController@index'); // 一次撈全部
		Route::get('memberapis/{memberapi}', 'MemberapiController@show'); // 取得會員資料
		Route::post('memberapis', 'MemberapiController@store'); // 新增會員
		Route::put('memberapis', 'MemberapiController@update'); // 更新會員資料
		Route::delete('memberapis/{memberapi}', 'MemberapiController@delete'); // 刪除會員
		Route::delete('deletenote/{memberapi}', 'MemberapiController@deletenote'); // 刪除會員備註
	});

	//friend_api
	Route::group(['middleware' => 'auth:api'], function() {
		// Route::get('friendapis', 'FriendapiController@index'); // 一次撈全部
		Route::get('friendapis/{friendapi}', 'FriendapiController@show'); // 依會員取得好友資料
		Route::post('friendapis', 'FriendapiController@store'); // 新增好友
		Route::put('friendapis', 'FriendapiController@update'); // 更新好友資料
		Route::delete('friendapis/{friendapi}', 'FriendapiController@delete'); // 刪除好友
	});

	//survey_api
	Route::group(['middleware' => 'auth:api'], function() {
		// Route::get('surveyapis', 'SurveyapiController@index'); // 一次撈全部
		Route::get('surveyapis/{surveyapi}', 'SurveyapiController@show'); // 取得滿意度
		Route::post('surveyapis', 'SurveyapiController@store'); // 新增滿意度
		Route::put('surveyapis', 'SurveyapiController@update'); // 更新滿意度
		Route::delete('surveyapis/{surveyapi}', 'SurveyapiController@delete'); // 刪除滿意度
	});

	//orderlist_api
	Route::group(['middleware' => 'auth:api'], function() {
		// Route::get('orderlistapis', 'OrderlistapiController@index'); // 一次撈全部
		Route::get('orderlistapis/{orderlistapi}', 'OrderlistapiController@show'); // 取得訂單
		Route::get('orderlistdetail/{orderlistid}', 'OrderlistapiController@getorderlist'); // 取得訂單詳細資料
		Route::put('orderlistapis', 'OrderlistapiController@update'); // 修改現場訂單
		Route::delete('orderlistapis/{orderlistapi}', 'OrderlistapiController@delete'); // 刪除現場訂單
	});

	Route::post('register', 'Auth\RegisterController@register'); // 註冊
	Route::post('login', 'Auth\LoginController@login'); //登入
	Route::post('logout', 'Auth\LoginController@logout'); //登出

	// Auth::guard('api')->user(); // 登录用户实例
	// Auth::guard('api')->check(); // 用户是否登录
	// Auth::guard('api')->id(); // 登录用户ID

	//mix_api
	Route::group(['middleware' => 'auth:api'], function() {
		Route::get('telephoneapis/{telephone}', 'MemberapiController@Gettelephone'); // %telephone%
		Route::get('cardidapis/{cardidapis}', 'MemberapiController@Getcardid'); // %card_id%
		Route::get('emailapis/{emailapis}', 'MemberapiController@Getemail'); // %email%
		Route::get('nameapis/{nameapis}', 'MemberapiController@Getname'); // %name%
		Route::put('noteapis', 'MemberapiController@Putnote'); // put member note
		Route::put('orderlistnoteapis', 'OrderlistapiController@Putnote'); // put orderlist note
		Route::get('testv2getorder/{orderlistid}', 'OrderlistapiController@GetOrderListProduct'); // 取得訂單資料 v2.0
		Route::delete('deleteorderlistnote/{orderlistnote}', 'OrderlistapiController@delnote'); // 刪除備註
		Route::get('getone/{friendapi}', 'FriendapiController@getonefriend'); // 取得單一好友資料
		Route::get('getpage/{how}/{where}/{for}', 'MemberapiController@GetPage'); // search v2.0 (search,分頁,排序)
	});

});



