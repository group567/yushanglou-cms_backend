<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Friendapi extends Model
{
    protected $table = 'friend';

	protected $primaryKey = 'friend_id';

	protected $fillable = ['friend_id', 'customer_id ', 'relationship', 'lastname', 'firstname', 'birthday', 'telephone', 'email', 'address', 'be_customer', 'card_id'];
}
