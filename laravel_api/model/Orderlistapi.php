<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orderlistapi extends Model
{
    protected $table = 'orderlist';

	protected $primaryKey = 'orderlist_id';

	protected $fillable = ['orderlist_id', 'customer_id', 'add_time', 'department', 'order_way', 'type', 'amount', 'detail_id', 'tax', 'total', 'note'];
}
