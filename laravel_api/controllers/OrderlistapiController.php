<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Orderlistapi;
use DB;
use DateTime;
use Illuminate\Support\Facades\Input;

class OrderlistapiController extends Controller
{

    public function show($orderlistid)
    {

        $order=DB::table('orderlist')
            ->where('customer_id',$orderlistid)
            ->get();

        return $order;

    }

	// 取得訂單資料(點餐明細)
    public function getorderlist($orderlistid){
        
        $order=DB::table('orderlist')
        		->join('detail', 'orderlist.orderlist_id', '=', 'detail.orderlist_id')
        		->where('orderlist.orderlist_id',$orderlistid)
        		->first();
        // print_r($order->item);

        if($order){

            // 切割 item
            $str_sec = explode("/",$order->item);
            // print_r($str_sec);
            // echo $str_sec[0];

            // 搜尋 item
            $myArray = array();
            $j=count($str_sec);

            for($i=0; $i<$j; $i++){
                $reslut = DB::table('menu')
                        ->where('meal_id',$str_sec[$i])
                        ->first();

                array_push($myArray, $reslut); 
            }
            $order->item = $myArray;
            // print_r($order);

            // 切割 price
            $str_price = explode("/",$order->price);
            $order->price = $str_price;

            // 切割 quantity
            $str_qua = explode("/",$order->quantity);
            $order->quantity = $str_qua;

            return response()->json($order, 200);

        }else{

            $json["status"]="-1";
            $json["data"]="Data not found.";
            $json_ouput=json_encode($json);
            return $json_ouput;

        }
    }

    // 修改現場訂單
    public function update(Request $request)
    {
        // $orderlistapi->update($request->all());

        // return response()->json($orderlistapi, 200);

    	$data = (json_decode($request->getContent(), true));
    	$now = new DateTime();

    	$article = DB::table('orderlist')->where('orderlist_id',$data["orderlist_id"])->update([
    	    'add_time' => $data['add_time'],
	    	'department' => $data['department'],
	    	// 'order_way' => $data['order_way'],
		    'type' => $data['type'],
		    // 'amount' => $data['amount'],
		    // 'detail_id' => $data['detail_id'],
		    // 'tax' => $data['tax'],
		    // 'total' => $data['total'],
		    // 'note' => $data['note'],
	    	'updated_at' => $now
    	]);

    	$sel = DB::table('orderlist')
    	    ->where('orderlist_id',$data["orderlist_id"])
    	    ->get();

    	return response()->json($sel, 200);

    }

    // 刪除現場訂單
    public function delete(Orderlistapi $orderlistapi)
    {

        $data = (json_decode($orderlistapi, true));
        // print_r($data);
        // echo $data['orderlist_id'];
        DB::table('orderlist')->where('orderlist_id',$data['orderlist_id'])->delete();
        DB::table('detail')->where('orderlist_id',$data['orderlist_id'])->delete();
        DB::table('survey')->where('orderlist_id',$data['orderlist_id'])->delete();

        return response()->json(null, 204);

    }

    // put note update
    public function Putnote(Request $request)
    {
        
        $data = (json_decode($request->getContent(), true));
        // print_r($data);

        $now = new DateTime();

        $memberapi = DB::table('orderlist')->where('orderlist_id',$data["orderlist_id"])->update([
                'note' => $data['note'],
                'updated_at' => $now
        ]);

        $sel = DB::table('orderlist')
                    ->where('orderlist_id',$data['orderlist_id'])
                    ->first();

        return response()->json($sel, 200);

    }

    // 取得訂單資料 v2.0
    public function GetOrderListProduct($orderlistid){
        
        $order=DB::table('orderlist')
                ->join('detail', 'orderlist.orderlist_id', '=', 'detail.orderlist_id')
                ->where('orderlist.orderlist_id',$orderlistid)
                ->first();

        if($order){

            // 切割 item
            $str_sec = explode("/",$order->item);

            // 搜尋 item
            $myArray = array();
            $j=count($str_sec);

            for($i=0; $i<$j; $i++){
                $reslut = DB::table('product')
                        ->join('product_name', 'product.product_id', '=', 'product_name.product_id')
                        ->join('product_quantity', 'product.product_id', '=', 'product_quantity.product_id')
                        ->where('product.product_id',$str_sec[$i])
                        ->first();
                        // ->get(['img','price','tax','note','simplified_chinese','english','vietnamese','quantity_ch','quantity_en','quantity_vi']);

                // // 切割 quantity_ch
                // $str_two = explode("/",$reslut->quantity_ch);
                // $reslut->quantity_ch = $str_two;

                // // 切割 quantity_en
                // $str_two = explode("/",$reslut->quantity_en);
                // $reslut->quantity_en = $str_two;

                // // 切割 quantity_vi
                // $str_two = explode("/",$reslut->quantity_vi);
                // $reslut->quantity_vi = $str_two;

                array_push($myArray, $reslut);
            }
            $order->item = $myArray;

            // 切割 price
            $str_price = explode("/",$order->price);
            $order->price = $str_price;

            // 切割 quantity
            $str_qua = explode("/",$order->quantity);
            $order->quantity = $str_qua;

           

            return response()->json($order, 200);

        }else{

            $json["status"]="-1";
            $json["data"]="Data not found.";
            $json_ouput=json_encode($json);
            return $json_ouput;

        }
    }

    // 刪除備註
    public function delnote(Orderlistapi $orderlistnote)
    {
        
        $data = (json_decode($orderlistnote, true));

        $now = new DateTime();

        $newnote = "";

        $orderlistnote = DB::table('orderlist')->where('orderlist_id',$data["orderlist_id"])->update([
                'note' => $newnote,
                'updated_at' => $now
        ]);

        return response()->json(null, 204);
    }

}
