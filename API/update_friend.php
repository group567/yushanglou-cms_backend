<?php


//連線
include("sql_connect.php");


//更新會員API


$data = json_decode(file_get_contents('php://input'), true);

$friend_id = $data['friend_id'];
$customer_id = $data['customer_id']; //對應customer

$relationship = $data['relationship']; //關係
$lastname = $data['lastname'];
$firstname = $data['firstname'];
$birthday = $data['birthday'];
$telephone = $data['telephone']; 
$email = $data['email'];
$address = $data['address'];
$be_customer = $data['be_customer']; //是否會員:Y/N
$card_id = $data['card_id']; //會員卡號

$dt = new DateTime();
$update_time = $dt->format('Y-m-d H:i:s'); //新增時間





$data = [

    $relationship,
    $lastname,
    $firstname,
    $birthday,
    $telephone,
    $email,
    $address,
    $be_customer,
    $card_id,

    $update_time,

    $friend_id,
    $customer_id,
];






// Step1.判斷card_id,telephone有沒有改

//SELECT
$res = $conn->prepare("SELECT * FROM friend WHERE customer_id=?");
$res->execute([$customer_id]);
$res_arr = $res->fetch(PDO::FETCH_ASSOC);

if($res_arr['card_id'] == $card_id && $res_arr['telephone'] == $telephone){

	$sql = "UPDATE friend SET relationship=?, lastname=?, firstname=?, birthday=?, telephone=?, email=?, address=?, be_customer=?, card_id=?, update_time=? WHERE friend_id=? AND customer_id=?";
	$result = $conn->prepare($sql);
	$result->execute(array($relationship,$lastname,$firstname,$birthday,$telephone,$email,$address,$be_customer,$card_id,$update_time,$friend_id,$customer_id));

	if($result){

		$zz = json_encode(array(
		    'status' => 0,
		    'error_msg' => 'Update Successesful!'
		));

		echo $zz;
		return ;

	}

}else if($res_arr['card_id'] != null && $res_arr['card_id'] != $card_id && $res_arr['telephone'] == $telephone){

	//SELECT 判斷會員卡號
	$rs = $conn->prepare("SELECT * FROM friend WHERE card_id=?");
	$rs->execute([$card_id]);
	$result_arr = $rs->fetchAll();

	if($result_arr != null){

		echo json_encode(array(
		    'status' => -1,
		    'error_msg' => 'Update Failed!Card_id has been used!'
		));

		return ;

	}else{

		$sql = "UPDATE friend SET relationship=?, lastname=?, firstname=?, birthday=?, telephone=?, email=?, address=?, be_customer=?, card_id=?, update_time=? WHERE friend_id=? AND customer_id=?";
		$result = $conn->prepare($sql);
		$result->execute(array($relationship,$lastname,$firstname,$birthday,$telephone,$email,$address,$be_customer,$card_id,$update_time,$friend_id,$customer_id));

		if($result){

			$zz = json_encode(array(
			    'status' => 0,
			    'error_msg' => 'Update Successesful!'
			));

			echo $zz;
			return ;

		}

	}

}else if($res_arr['telephone'] != $telephone && $res_arr['card_id'] == $card_id){

	// //SELECT 判斷手機
	$asd = $conn->prepare("SELECT * FROM friend WHERE telephone=?");
	$asd->execute([$telephone]);
	$zxc = $asd->fetchAll();
	// // echo json_encode($zxc);

	if($zxc != null){

		echo json_encode(array(
		    'status' => -2,
		    'error_msg' => 'Update Failed!Telephone has been used!'
		));

		return ;

	}else{

		$sql = "UPDATE friend SET relationship=?, lastname=?, firstname=?, birthday=?, telephone=?, email=?, address=?, be_customer=?, card_id=?, update_time=? WHERE friend_id=? AND customer_id=?";
		$result = $conn->prepare($sql);
		$result->execute(array($relationship,$lastname,$firstname,$birthday,$telephone,$email,$address,$be_customer,$card_id,$update_time,$friend_id,$customer_id));

		if($result){

			$zz = json_encode(array(
			    'status' => 0,
			    'error_msg' => 'Update Successesful!'
			));

			echo $zz;
			return ;

		}

	}

}else if($res_arr['card_id'] != null && $res_arr['card_id'] != $card_id && $res_arr['telephone'] != $telephone){

	//SELECT 判斷會員卡號
	$rs = $conn->prepare("SELECT * FROM friend WHERE card_id=?");
	$rs->execute([$card_id]);
	$result_arr = $rs->fetchAll();

	// //SELECT 判斷手機
	$asd = $conn->prepare("SELECT * FROM friend WHERE telephone=?");
	$asd->execute([$telephone]);
	$zxc = $asd->fetchAll();

	if($result_arr != null){

		echo json_encode(array(
		    'status' => -1,
		    'error_msg' => 'Update Failed!Card_id has been used!'
		));
		return ;

	}else if($zxc != null){

		echo json_encode(array(
		    'status' => -2,
		    'error_msg' => 'Update Failed!Telephone has been used!'
		));
		return ;

	}else if($result_arr == null && $zxc == null){

		$sql = "UPDATE friend SET relationship=?, lastname=?, firstname=?, birthday=?, telephone=?, email=?, address=?, be_customer=?, card_id=?, update_time=? WHERE friend_id=? AND customer_id=?";
		$result = $conn->prepare($sql);
		$result->execute(array($relationship,$lastname,$firstname,$birthday,$telephone,$email,$address,$be_customer,$card_id,$update_time,$friend_id,$customer_id));

		if($result){

			$zz = json_encode(array(
			    'status' => 0,
			    'error_msg' => 'Update Successesful!'
			));

			echo $zz;
			return ;

		}

	}


}else if($res_arr['card_id'] == null && $res_arr['card_id'] != $card_id && $res_arr['telephone'] != $telephone){
	
	$sql = "UPDATE friend SET relationship=?, lastname=?, firstname=?, birthday=?, telephone=?, email=?, address=?, be_customer=?, card_id=?, update_time=? WHERE friend_id=? AND customer_id=?";
	$result = $conn->prepare($sql);
	$result->execute(array($relationship,$lastname,$firstname,$birthday,$telephone,$email,$address,$be_customer,$card_id,$update_time,$friend_id,$customer_id));

	if($result){

		$zz = json_encode(array(
		    'status' => 0,
		    'error_msg' => 'Update Successesful!'
		));

		echo $zz;
		return ;

	}

}













?>