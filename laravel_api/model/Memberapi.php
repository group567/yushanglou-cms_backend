<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Memberapi extends Model
{
	protected $table = 'customer';

	protected $primaryKey = 'customer_id';

    protected $fillable = ['customer_id ', 'card_id', 'lastname', 'firstname', 'birthday', 'telephone', 'email', 'address', 'company', 'established', 'business_attribute', 'note', 'consumption_times', 'cost_total'];
}
