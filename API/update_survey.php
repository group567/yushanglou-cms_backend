<?php


//連線
include("sql_connect.php");


//更新滿意度API


$data = json_decode(file_get_contents('php://input'), true);

$survey_id = $data['survey_id']; //
$orderlist_id = $data['orderlist_id']; //對應order

$visits_times = $data['visits_times']; //次數
$pipeline = $data['pipeline']; //管道
$purpose = $data['purpose']; //目的
$people = $data['people']; //人數
$attitude = $data['attitude']; //態度滿意度
$effectiveness = $data['effectiveness']; //速度
$introduction = $data['introduction']; //介紹
$note = $data['note'];

$dt = new DateTime();
$update_time = $dt->format('Y-m-d H:i:s'); //新增時間





$data = [

    $visits_times,
    $pipeline,
    $purpose,
    $people,
    $attitude,
    $effectiveness,
    $introduction,
    $note,

    $update_time,

    $survey_id,
    $orderlist_id,
];







//SELECT
$res = $conn->prepare("SELECT * FROM survey WHERE survey_id=? AND orderlist_id=?");
$res->execute([$survey_id,$orderlist_id]);
$res_arr = $res->fetch(PDO::FETCH_ASSOC);

if($res_arr){

	$sql = "UPDATE survey SET visits_times=?, pipeline=?, purpose=?, people=?, attitude=?, effectiveness=?, introduction=?, note=?, update_time=? WHERE survey_id=? AND orderlist_id=?";
	$result = $conn->prepare($sql);
	$result->execute(array($visits_times,$pipeline,$purpose,$people,$attitude,$effectiveness,$introduction,$note,$update_time,$survey_id,$orderlist_id));

	if($result){

		$zz = json_encode(array(
		    'status' => 0,
		    'error_msg' => 'Update Successesful!'
		));

		echo $zz;
		return ;

	}

}else{

	echo json_encode(array(
	    'status' => -1,
	    'error_msg' => 'Update Failed.'
	));

}

	










?>