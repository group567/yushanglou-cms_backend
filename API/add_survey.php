<?php


//連線
include("sql_connect.php");


//新增滿意度 API


$data = json_decode(file_get_contents('php://input'), true);

$orderlist_id = $data['orderlist_id']; //對應order
$visits_times = $data['visits_times']; //次數
$pipeline = $data['pipeline']; //管道
$purpose = $data['purpose']; //目的
$people = $data['people']; //人數

$attitude = $data['attitude']; //態度滿意度
$effectiveness = $data['effectiveness']; //速度
$introduction = $data['introduction']; //介紹
$note = $data['note'];

$dt = new DateTime();
$add_time = $dt->format('Y-m-d H:i:s'); //新增時間



$data = [
    $orderlist_id,
    $visits_times,
    $pipeline,
    $purpose,
    $people,
    $attitude,
    $effectiveness,
    $introduction,
    $note,

    $add_time,
];



// //SELECT
$rs = $conn->prepare("SELECT * FROM survey WHERE orderlist_id=?");
$rs->execute([$orderlist_id]);
$row = $rs->fetch(PDO::FETCH_ASSOC);




if($row == null){

    //INSERT
    $rs = $conn->prepare("INSERT INTO survey ( orderlist_id,visits_times,pipeline,purpose,people,attitude,effectiveness,introduction,note,add_time) VALUES (?,?,?,?,?,?,?,?,?,?)");
    $rs->execute($data);

	echo json_encode(array(
	    'status' => 0,
	    'error_msg' => 'Add Successesful.'
	));


}else{

	echo json_encode(array(
	    'status' => -1,
	    'error_msg' => 'Have Data! Add Failed.'
	));

}
































?>