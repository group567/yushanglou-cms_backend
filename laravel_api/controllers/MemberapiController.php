<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Memberapi;
use DB;
use DateTime;
use Illuminate\Support\Facades\Input;

class MemberapiController extends Controller
{
    // 取全部會員
    public function index()
    {
        return Memberapi::all();
    }

    // 取得會員資料 -> 需改寫
    public function show(Memberapi $memberapi)
    {
        return $memberapi;
        
        // $data = (json_decode($memberapi, true));
        // print_r($data);
        // echo $data['orderlist_id'];
        // $sel = DB::table('friend')
        //     ->where('customer_id',$data['customer_id'])
        //     ->get();

        // return response()->json($sel, 200);

    }

    // 新增會員
    public function store(Request $request)
    {
    	
    	$data = (json_decode($request->getContent(), true));

    	$ser = DB::table('customer')
                ->where('card_id',$data['card_id'])
                ->orWhere('telephone',$data['telephone'])
                ->first();

        if($ser != ""){

        	$json["status"]="-1";
    		$json["data"]="Card_id or Telephone has be used.";
    		$json_ouput=json_encode($json);
    		return $json_ouput;

        }else{

        	$now = new DateTime();
            $data['note'] = "";
            $data['consumption_times'] = null;
            $data['cost_total'] = null;
            if($data['company'] == ""){
                $data['company'] = "";
            }
            if($data['established'] == ""){
                $data['established'] = null;
            }
            if($data['business_attribute'] == ""){
                $data['business_attribute'] = "";
            }

        	$memberapi = DB::table('customer')->insert(
        	    array(
        	    	'card_id' => $data['card_id'],
        	    	'lastname' => $data['lastname'],
        		    'firstname' => $data['firstname'],
        		    'birthday' => $data['birthday'],
        		    'telephone' => $data['telephone'],
        		    'email' => $data['email'],
        		    'address' => $data['address'],
        		    'company' => $data['company'],
        		    'established' => $data['established'],
        		    'business_attribute' => $data['business_attribute'],
        		    'note' => $data['note'],
        		    'consumption_times' => $data['consumption_times'],
        		    'cost_total' => $data['cost_total'],
        	    	'updated_at' => $now,
        	    	'created_at' => $now
        	    	)
        	);

        	$sel = DB::table('customer')
        	                ->where('card_id',$data['card_id'])
        	                ->get();

        	return response()->json($sel, 201);

        }

    }

    // 更新會員資料
    public function update(Request $request)
    {
        
        $data = (json_decode($request->getContent(), true));

        $ser = DB::table('customer')
                ->where('customer_id',$data['customer_id'])
                ->first();
                // print_r($ser);
                // echo $ser->card_id;
        if($ser->card_id == $data['card_id'] && $ser->telephone == $data['telephone']){ // card_id,telephone沒有改

            $now = new DateTime();
            if($data['company'] == ""){
                $data['company'] = "";
            }
            if($data['established'] == ""){
                $data['established'] = null;
            }
            if($data['business_attribute'] == ""){
                $data['business_attribute'] = "";
            }

            $memberapi = DB::table('customer')->where('customer_id',$data["customer_id"])->update([
                    'card_id' => $data['card_id'],
                    'lastname' => $data['lastname'],
                    'firstname' => $data['firstname'],
                    'birthday' => $data['birthday'],
                    'telephone' => $data['telephone'],
                    'email' => $data['email'],
                    'address' => $data['address'],
                    'company' => $data['company'],
                    'established' => $data['established'],
                    'business_attribute' => $data['business_attribute'],
                    'updated_at' => $now
            ]);

            $sel = DB::table('customer')
                        ->where('customer_id',$data['customer_id'])
                        ->first();

            return response()->json($sel, 200);

        }else if($ser->card_id != $data['card_id'] && $ser->telephone == $data['telephone']){ // card_id改,查card_id

            $res = DB::table('customer')
                ->where('card_id',$data['card_id'])
                ->first();
                
            if($res != ""){

                $json["status"]="-1";
                $json["data"]="Card_id has be used.";
                $json_ouput=json_encode($json);
                return $json_ouput;

            }else{

                $now = new DateTime();

                $memberapi = DB::table('customer')->where('customer_id',$data["customer_id"])->update([
                    'card_id' => $data['card_id'],
                    'lastname' => $data['lastname'],
                    'firstname' => $data['firstname'],
                    'birthday' => $data['birthday'],
                    'telephone' => $data['telephone'],
                    'email' => $data['email'],
                    'address' => $data['address'],
                    'company' => $data['company'],
                    'established' => $data['established'],
                    'business_attribute' => $data['business_attribute'],
                    'updated_at' => $now
                ]);

                $sel = DB::table('customer')
                            ->where('customer_id',$data['customer_id'])
                            ->first();

                return response()->json($sel, 200);

            }

        }else if($ser->telephone != $data['telephone'] && $ser->card_id == $data['card_id']){ // telephone改,查telephone

            $ref = DB::table('customer')
                ->where('telephone',$data['telephone'])
                ->first();

            if($ref != ""){

                $json["status"]="-1";
                $json["data"]="Telephone has be used.";
                $json_ouput=json_encode($json);
                return $json_ouput;

            }else{

                $now = new DateTime();

                $memberapi = DB::table('customer')->where('customer_id',$data["customer_id"])->update([
                    'card_id' => $data['card_id'],
                    'lastname' => $data['lastname'],
                    'firstname' => $data['firstname'],
                    'birthday' => $data['birthday'],
                    'telephone' => $data['telephone'],
                    'email' => $data['email'],
                    'address' => $data['address'],
                    'company' => $data['company'],
                    'established' => $data['established'],
                    'business_attribute' => $data['business_attribute'],
                    'updated_at' => $now
                ]);

                $sel = DB::table('customer')
                            ->where('customer_id',$data['customer_id'])
                            ->first();

                return response()->json($sel, 200);

            }

        }else{ // 兩者改,兩者都查

            $rrs = DB::table('customer')
                ->where('card_id',$data['card_id'])
                ->orWhere('telephone',$data['telephone'])
                ->first();

            if($rrs != ""){

                $json["status"]="-2";
                $json["data"]="Card_id or Telephone has be used.";
                $json_ouput=json_encode($json);
                return $json_ouput;

            }else{

                $now = new DateTime();

                $memberapi = DB::table('customer')->where('customer_id',$data["customer_id"])->update([
                    'card_id' => $data['card_id'],
                    'lastname' => $data['lastname'],
                    'firstname' => $data['firstname'],
                    'birthday' => $data['birthday'],
                    'telephone' => $data['telephone'],
                    'email' => $data['email'],
                    'address' => $data['address'],
                    'company' => $data['company'],
                    'established' => $data['established'],
                    'business_attribute' => $data['business_attribute'],
                    'updated_at' => $now
                ]);

                $sel = DB::table('customer')
                            ->where('customer_id',$data['customer_id'])
                            ->first();

                return response()->json($sel, 200);

            }

        }

    }

    // 刪除會員
    public function delete(Memberapi $memberapi)
    {
        // $memberapi->delete();

        $data = (json_decode($memberapi, true));

        DB::table('customer')->where('customer_id',$data['customer_id'])->delete();
        DB::table('friend')->where('customer_id',$data['customer_id'])->delete();

        // select orderlist -> del detail,survey -> del orderlist
        $res = DB::table('orderlist')
            ->where('customer_id',$data['customer_id'])
            ->get();

        // print_r($res);
        // print_r($res[0]->orderlist_id);

        $j = count($res);

        for($i=0; $i<$j; $i++){
           DB::table('detail')->where('orderlist_id',$res[$i]->orderlist_id)->delete();
           DB::table('survey')->where('orderlist_id',$res[$i]->orderlist_id)->delete();
        }

        DB::table('orderlist')->where('customer_id',$data['customer_id'])->delete();

        return response()->json(null, 204);
    }

    // get memberlist -> %telephone%
    public function Gettelephone($telephone){
        
        $order=DB::table('customer')->where('telephone','LIKE','%'.$telephone.'%')->orderBy('customer_id', 'desc')->get();

        $i=count($order);
        
        if($i != 0){

            return response()->json($order, 200);

        }else{

            $json["status"]="-1";
            $json["data"]="Data not found.";
            // $json_ouput=json_encode($json);
            // return $json_ouput;
            return response()->json($json, 200);

            // $json_ouput=$json;
            // return response()->json($json_ouput, 404);

        }
    }

    // get memberlist -> %card_id%
    public function Getcardid($cardid){
        
        $order=DB::table('customer')->where('card_id','LIKE','%'.$cardid.'%')->orderBy('customer_id', 'desc')->get();

        $i=count($order);
        
        if($i != 0){

            return response()->json($order, 200);

        }else{

            $json["status"]="-1";
            $json["data"]="Data not found.";
            return response()->json($json, 200);
        }
    }

    // get memberlist -> %email%
    public function Getemail($email){
        
        $order=DB::table('customer')->where('email','LIKE','%'.$email.'%')->orderBy('customer_id', 'desc')->get();

        $i=count($order);
        
        if($i != 0){

            return response()->json($order, 200);

        }else{

            $json["status"]="-1";
            $json["data"]="Data not found.";
            return response()->json($json, 200);

        }
    }

    // get memberlist -> %name%
    public function Getname($name){
        
        $order=DB::table('customer')->where('lastname','LIKE','%'.$name.'%')->orwhere('firstname','LIKE','%'.$name.'%')->orderBy('customer_id', 'desc')->get();

        $i=count($order);
        
        if($i != 0){

            return response()->json($order, 200);

        }else{

            $json["status"]="-1";
            $json["data"]="Data not found.";
            return response()->json($json, 200);

        }
    }

    // put note update
    public function Putnote(Request $request)
    {
        
        $data = (json_decode($request->getContent(), true));
        // print_r($data);

        $now = new DateTime();

        $memberapi = DB::table('customer')->where('customer_id',$data["customer_id"])->update([
                'note' => $data['note'],
                'updated_at' => $now
        ]);

        $sel = DB::table('customer')
                    ->where('customer_id',$data['customer_id'])
                    ->first();

        return response()->json($sel, 200);

    }

    // 刪除會員備註
    public function deletenote(Memberapi $memberapi)
    {
        
        $data = (json_decode($memberapi, true));

        $now = new DateTime();

        $newnote = "";

        $memberapi = DB::table('customer')->where('customer_id',$data["customer_id"])->update([
                'note' => $newnote,
                'updated_at' => $now
        ]);

        return response()->json(null, 204);
    }

    // search v2.0
    public function GetPage($how, $where, $for){

        if($how == "name"){

            $order=DB::table('customer')->where('lastname','LIKE','%'.$where.'%')->orwhere('firstname','LIKE','%'.$where.'%')->orderBy('customer_id', $for)->paginate(10);

        }else{

            $order=DB::table('customer')->where($how,'LIKE','%'.$where.'%')->orderBy('customer_id', $for)->paginate(10);

        }
        
        $i=count($order);
        
        if($i != 0){

            return response()->json($order, 200);

        }else{

            $json["status"]="-1";
            $json["data"]="Data not found.";
            return response()->json($json, 200);

        }
    }


}
