<?php


//連線
include("sql_connect.php");


//新增會員API

$data = json_decode(file_get_contents('php://input'), true);


$customer_id = $data['customer_id']; //id

$card_id = $data['card_id']; //卡號
$lastname = $data['lastname']; //姓
$firstname = $data['firstname']; //名
$birthday = $data['birthday'];
$telephone = $data['telephone'];
$email = $data['email']; 
$address = $data['address']; 
$company = $data['company']; //公司名稱
$established = $data['established']; //成立時間
$business_attribute = $data['business_attribute']; //業務屬性

// $ip = $_SERVER["REMOTE_ADDR"]; //ip
$note = $data['note']; //備註
$consumption_times = $data['consumption_times']; //消費次數
$cost_total = $data['cost_total']; //消費總額

$dt = new DateTime();
$update_time = $dt->format('Y-m-d H:i:s'); //新增時間



$data = [
    

    $card_id,
    $lastname,
    $firstname,
    $birthday,
    $telephone,
    $email,
    $address,
    $company,
    $established,
    $business_attribute,

    $note,
    $consumption_times,
    $cost_total,
    $update_time,

    $customer_id,
];


// Step1.判斷card_id,telephone有沒有改

//SELECT
$res = $conn->prepare("SELECT * FROM customer WHERE customer_id=?");
$res->execute([$customer_id]);
$res_arr = $res->fetch(PDO::FETCH_ASSOC);

if($res_arr['card_id'] == $card_id && $res_arr['telephone'] == $telephone){

	$sql = "UPDATE customer SET card_id=?, lastname=?, firstname=?, birthday=?, telephone=?, email=?, address=?, company=?, established=?, business_attribute=?, note=?, consumption_times=?, cost_total=?, update_time=? WHERE customer_id=?";
	$result = $conn->prepare($sql);
	$result->execute(array($card_id,$lastname,$firstname,$birthday,$telephone,$email,$address,$company,$established,$business_attribute,$note,$consumption_times,$cost_total,$update_time,$customer_id));

	if($result){

		$zz = json_encode(array(
		    'status' => 0,
		    'error_msg' => 'Update Successesful!'
		));

		echo $zz;
		return ;

	}

}else if($res_arr['card_id'] != $card_id && $res_arr['telephone'] == $telephone){

	//SELECT 判斷會員卡號
	$rs = $conn->prepare("SELECT * FROM customer WHERE card_id=?");
	$rs->execute([$card_id]);
	$result_arr = $rs->fetchAll();

	if($result_arr != null){

		echo json_encode(array(
		    'status' => -1,
		    'error_msg' => 'Update Failed!Card_id has been used!'
		));

		return ;

	}else{

		$sql = "UPDATE customer SET card_id=?, lastname=?, firstname=?, birthday=?, telephone=?, email=?, address=?, company=?, established=?, business_attribute=?, note=?, consumption_times=?, cost_total=?, update_time=? WHERE customer_id=?";
			$result = $conn->prepare($sql);
			$result->execute(array($card_id,$lastname,$firstname,$birthday,$telephone,$email,$address,$company,$established,$business_attribute,$note,$consumption_times,$cost_total,$update_time,$customer_id));

			if($result){

				$zz = json_encode(array(
				    'status' => 0,
				    'error_msg' => 'Update Successesful!'
				));

				echo $zz;
				return ;

			}

	}

}else if($res_arr['telephone'] != $telephone && $res_arr['card_id'] == $card_id){

	// //SELECT 判斷手機
	$asd = $conn->prepare("SELECT * FROM customer WHERE telephone=?");
	$asd->execute([$telephone]);
	$zxc = $asd->fetchAll();
	// // echo json_encode($zxc);

	if($zxc != null){

		echo json_encode(array(
		    'status' => -2,
		    'error_msg' => 'Update Failed!Telephone has been used!'
		));

		return ;

	}else{

		$sql = "UPDATE customer SET card_id=?, lastname=?, firstname=?, birthday=?, telephone=?, email=?, address=?, company=?, established=?, business_attribute=?, note=?, consumption_times=?, cost_total=?, update_time=? WHERE customer_id=?";
			$result = $conn->prepare($sql);
			$result->execute(array($card_id,$lastname,$firstname,$birthday,$telephone,$email,$address,$company,$established,$business_attribute,$note,$consumption_times,$cost_total,$update_time,$customer_id));

			if($result){

				$zz = json_encode(array(
				    'status' => 0,
				    'error_msg' => 'Update Successesful!'
				));

				echo $zz;
				return ;

			}

	}

}else if($res_arr['card_id'] != $card_id && $res_arr['telephone'] != $telephone){

	//SELECT 判斷會員卡號
	$rs = $conn->prepare("SELECT * FROM customer WHERE card_id=?");
	$rs->execute([$card_id]);
	$result_arr = $rs->fetchAll();

	// //SELECT 判斷手機
	$asd = $conn->prepare("SELECT * FROM customer WHERE telephone=?");
	$asd->execute([$telephone]);
	$zxc = $asd->fetchAll();

	if($result_arr != null){

		echo json_encode(array(
		    'status' => -1,
		    'error_msg' => 'Update Failed!Card_id has been used!'
		));
		return ;

	}else if($zxc != null){

		echo json_encode(array(
		    'status' => -2,
		    'error_msg' => 'Update Failed!Telephone has been used!'
		));
		return ;

	}else if($result_arr == null && $zxc == null){

		$sql = "UPDATE customer SET card_id=?, lastname=?, firstname=?, birthday=?, telephone=?, email=?, address=?, company=?, established=?, business_attribute=?, note=?, consumption_times=?, cost_total=?, update_time=? WHERE customer_id=?";
			$result = $conn->prepare($sql);
			$result->execute(array($card_id,$lastname,$firstname,$birthday,$telephone,$email,$address,$company,$established,$business_attribute,$note,$consumption_times,$cost_total,$update_time,$customer_id));

			if($result){

				$zz = json_encode(array(
				    'status' => 0,
				    'error_msg' => 'Update Successesful!'
				));

				echo $zz;
				return ;

			}

	}


}




















































?>