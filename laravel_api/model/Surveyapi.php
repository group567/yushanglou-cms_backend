<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Surveyapi extends Model
{
    protected $table = 'survey';

	protected $primaryKey = 'survey_id';

	protected $fillable = ['survey_id', 'orderlist_id', 'visits_times', 'pipeline', 'purpose', 'people', 'attitude', 'effectiveness', 'introduction', 'note'];
}
