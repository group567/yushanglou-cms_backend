<?php


//連線
include("sql_connect.php");


//新增會員API

$data = json_decode(file_get_contents('php://input'), true);


$card_id = $data['card_id']; //卡號
$lastname = $data['lastname']; //姓
$firstname = $data['firstname']; //名
$birthday = $data['birthday'];
$telephone = $data['telephone'];
$email = $data['email']; 
$address = $data['address']; 
$company = $data['company']; //公司名稱
$established = $data['established']; //成立時間
$business_attribute = $data['business_attribute']; //業務屬性

// $ip = $_SERVER["REMOTE_ADDR"]; //ip
$note = $data['note']; //備註
$consumption_times = $data['consumption_times']; //消費次數
$cost_total = $data['cost_total']; //消費總額

$dt = new DateTime();
$add_time = $dt->format('Y-m-d H:i:s'); //新增時間



$data = [
    $card_id,
    $lastname,
    $firstname,
    $birthday,
    $telephone,
    $email,
    $address,
    $company,
    $established,
    $business_attribute,

    $note,
    $consumption_times,
    $cost_total,
    $add_time,
];




//SELECT 判斷會員卡號
$rs = $conn->prepare("SELECT * FROM customer WHERE card_id=?");
$rs->execute([$card_id]);
$result_arr = $rs->fetchAll();
// echo json_encode($result_arr);



// //SELECT 判斷手機
$asd = $conn->prepare("SELECT * FROM customer WHERE telephone=?");
$asd->execute([$telephone]);
$zxc = $asd->fetchAll();
// // echo json_encode($zxc);

if($result_arr != null){

	echo json_encode(array(
	    'status' => -1,
	    'error_msg' => 'Card_id has be used! Please try again!'
	));

}else if($zxc != null){

	echo json_encode(array(
	    'status' => -2,
	    'error_msg' => 'Telephonenumber has be used! Please try again!'
	));

}else if($result_arr == null && $zxc == null){

	
	//INSERT
	$rs = $conn->prepare("INSERT INTO customer ( card_id, lastname, firstname, birthday, telephone, email, address, company, established, business_attribute, note, consumption_times, cost_total, add_time) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
	$rs->execute($data);


	//SELECT
	$rs = $conn->prepare("SELECT * FROM customer WHERE card_id=?");
	$rs->execute([$card_id]);
	$row = $rs->fetch(PDO::FETCH_ASSOC);
	// echo json_encode($result_arr);

	

	echo json_encode(array(
	    'status' => 0,
	    'error_msg' => 'Add Successesful.',
	    'data' => $row
	));

}





















?>