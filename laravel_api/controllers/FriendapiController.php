<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Friendapi;
use DB;
use DateTime;

class FriendapiController extends Controller
{
	// 取全部DB好友
    public function index()
    {
        return Friendapi::all();
    }

    // 取得好友資料
    public function show($friendapi)
    {
    	// return $friendapi;

        $data = (json_decode($friendapi, true));
        // print_r($data);
        // echo $data['orderlist_id'];
        $sel = DB::table('friend')
            ->where('customer_id',$data)
            ->get();

        return response()->json($sel, 200);

    }

    // 新增好友
    public function store(Request $request)
    {
    	
        // $friendapi = Friendapi::create($request->all());
        // return response()->json($friendapi, 201);

    	$data = (json_decode($request->getContent(), true));

    	$now = new DateTime();

        if($data['card_id'] == ""){
            $data['be_customer'] = "n";
        }

    	$friendapi = DB::table('friend')->insert(
    	    array(
    	    	'customer_id' => $data['customer_id'],
    	    	'relationship' => $data['relationship'],
    	    	'lastname' => $data['lastname'],
    		    'firstname' => $data['firstname'],
    		    'birthday' => $data['birthday'],
    		    'telephone' => $data['telephone'],
    		    'email' => $data['email'],
    		    'address' => $data['address'],
    		    'be_customer' => $data['be_customer'],
    		    'card_id' => $data['card_id'],
    	    	'created_at' => $now
    	    	)
    	);

    	$sel = DB::table('friend')
    	                ->where('customer_id',$data['customer_id'])
    	                ->get();

    	return response()->json($sel, 201);

    }

    // 更新好友資料
    public function update(Request $request)
    {
        // $friendapi->update($request->all());

        // return response()->json($friendapi, 200);

    	$data = (json_decode($request->getContent(), true));
    	$now = new DateTime();

    	$article = DB::table('friend')->where('friend_id',$data["friend_id"])->update([
    	    'customer_id' => $data['customer_id'],
	    	'relationship' => $data['relationship'],
	    	'lastname' => $data['lastname'],
		    'firstname' => $data['firstname'],
		    'birthday' => $data['birthday'],
		    'telephone' => $data['telephone'],
		    'email' => $data['email'],
		    'address' => $data['address'],
		    'be_customer' => $data['be_customer'],
		    'card_id' => $data['card_id'],
	    	'updated_at' => $now
    	]);

    	$sel = DB::table('friend')
    	    ->where('friend_id',$data["friend_id"])
    	    ->get();

    	return response()->json($sel, 200);

    }

    // 刪除好友
    public function delete(Friendapi $friendapi)
    {
        $friendapi->delete();

        return response()->json(null, 204);
    }

}
