<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Surveyapi;
use DB;
use DateTime;
use Illuminate\Support\Facades\Input;

class SurveyapiController extends Controller
{
	// 取全部DB滿意度
    public function index()
    {
        return Surveyapi::all();
    }

    // 取得滿意度
    public function show($surveyapi)
    {
        $survey=DB::table('survey')
            ->where('orderlist_id',$surveyapi)
            ->get();

        return $survey;
    	// return $surveyapi;
    }

    // 新增滿意度
    public function store(Request $request)
    {
    	
        // $surveyapi = Surveyapi::create($request->all());
        // return response()->json($surveyapi, 201);

    	$data = (json_decode($request->getContent(), true));

    	$now = new DateTime();

    	$surveyapi = DB::table('survey')->insert([
    	    	'orderlist_id' => $data['orderlist_id'],
    	    	'visits_times' => $data['visits_times'],
    	    	'pipeline' => $data['pipeline'],
    		    'purpose' => $data['purpose'],
    		    'people' => $data['people'],
    		    'attitude' => $data['attitude'],
    		    'effectiveness' => $data['effectiveness'],
    		    'introduction' => $data['introduction'],
    		    'note' => $data['note'],
    	    	'created_at' => $now
    	]);

    	$sel = DB::table('survey')
    	        ->where('orderlist_id',$data['orderlist_id'])
    	        ->get();

    	return response()->json($sel, 201);

    }

    // 更新滿意度
    public function update(Request $request)
    {
        // $surveyapi->update($request->all());

        // return response()->json($surveyapi, 200);

    	$data = (json_decode($request->getContent(), true));
    	$now = new DateTime();

    	$article = DB::table('survey')->where('survey_id',$data["survey_id"])->update([
    	    'orderlist_id' => $data['orderlist_id'],
	    	'visits_times' => $data['visits_times'],
	    	'pipeline' => $data['pipeline'],
		    'purpose' => $data['purpose'],
		    'people' => $data['people'],
		    'attitude' => $data['attitude'],
		    'effectiveness' => $data['effectiveness'],
		    'introduction' => $data['introduction'],
		    'note' => $data['note'],
	    	'updated_at' => $now
    	]);

    	$sel = DB::table('survey')
    	    ->where('survey_id',$data["survey_id"])
    	    ->get();

    	return response()->json($sel, 200);

    }

    // 刪除滿意度
    public function delete(Surveyapi $surveyapi)
    {
        $surveyapi->delete();

        return response()->json(null, 204);
    }

}
